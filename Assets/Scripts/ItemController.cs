using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;



public class ItemController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    private AudioSource _audio;

    void Start()
    {
        _audio = gameObject.GetComponent<AudioSource>();
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        gameObject.transform.localScale *= 1.5f;
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        gameObject.transform.localScale = new Vector3(1, 1, 1);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _audio.Play();
    }
}
